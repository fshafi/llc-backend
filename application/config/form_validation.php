<?php
/**
 * Created by PhpStorm.
 * User: DELL OPTIPLEX 380
 * Date: 4/16/2015
 * Time: 3:29 PM
 */
$config = array('apiCalls/addAllInfo' => array(
    array('field' => 'State',
        'label' => 'State',
        'rules' => 'required'),

    array('field' => 'CompanyFName',
        'label' => 'CompanyFName',
        'rules' => 'required'),

    array('field' => 'CompanyLName',
        'label' => 'CompanyLName',
        'rules' => 'required'),

    array('field' => 'Street',
        'label' => 'Street',
        'Street' => 'required'),

    array('field' => 'City',
        'label' => 'City',
        'rules' => 'required'),

    array('field' => 'ZipCode',
        'label' => 'ZipCode',
        'rules' => 'required'),

    array('field' => 'Country',
        'label' => 'Country',
        'rules' => 'required'),

    array('field' => 'CompanySummary',
        'label' => 'CompanySummary',
        'rules' => 'required'),

    array('field' => 'BusinessCategoryName',
        'label' => 'BusinessCategoryName',
        'rules' => 'required'),

    array('field' => 'SelectPackage',
        'label' => 'SelectPackage',
        'rules' => 'required'),

    array('field' => 'PackageFee',
        'label' => 'PackageFee',
        'rules' => 'required'),

    array('field' => 'CorporationShare',
        'label' => 'CorporationShare',
        'Street' => 'required'),

    array('field' => 'Managed',
        'label' => 'Managed',
        'Street' => 'required'),

    array('field' => 'HideShareValue',
        'label' => 'HideShareValue',
        'Street' => 'required')),

//    array('field' => 'BusinessCategoryName',
//        'label' => 'BusinessCategoryName',
//        'rules' => 'required|min_length[1]|max_length[25]'),
//
//    array('field' => 'SelectPackage',
//        'label' => 'SelectPackage',
//        'rules' => 'required|min_length[1]|max_length[25]'),
//
//    array('field' => 'PackageFee',
//        'label' => 'PackageFee',
//        'rules' => 'required|min_length[1]|max_length[25]'),
//
//    array('field' => 'CorporationShare',
//        'label' => 'CorporationShare',
//        'Street' => 'required|min_length[1]|max_length[25]'),
//
//    array('field' => 'HideShareValue',
//        'label' => 'HideShareValue',
//        'Street' => 'required|min_length[1]|max_length[25]')),

    // memberinfo validation
//    'PostName' => $mPname,
//    'Name' => $mName,
//    'Street' => $mStreet,
//    'City' => $mCity,
//    'State' => $mState,
//    'ZipCode' => $mZipcode,
//    'Country' => $mCountry,
//    'Address' =>$mAddress,
//    'CId'=>$CId




    'apiCalls/a' => array(
        array('field' => 'BusinessCategoryName',
            'label' => 'BusinessCategoryName',
            'rules' => 'required|min_length[1]|max_length[25]'),

        array('field' => 'SelectPackage',
            'label' => 'SelectPackage',
            'rules' => 'required|min_length[1]|max_length[25]'),

        array('field' => 'PackageFee',
            'label' => 'PackageFee',
            'rules' => 'required|min_length[1]|max_length[25]'),

        array('field' => 'Managed',
            'label' => 'Managed',
            'rules' => 'required|min_length[1]|max_length[25]'),

        array('field' => 'CorporationShare',
            'label' => 'CorporationShare',
            'Street' => 'required|min_length[1]|max_length[25]'),

        array('field' => 'HideShareValue',
            'label' => 'HideShareValue',
            'Street' => 'required|min_length[1]|max_length[25]')),

//        array('field' => 'Expedite',
//            'label' => 'Expedite',
//            'rules' => 'required|min_length[1]|max_length[25]'),






    // Category validation
//    array('field' => 'BusinessCategoryName', 'label' => 'BusinessCategoryName', 'rules' => 'required|min_length[3]|max_length[25]'),
//    array('field' => 'SelectPackage', 'label' => 'SelectPackage', 'rules' => 'required|min_length[3]|max_length[25]'),
//    array('field' => 'PackageFee', 'label' => 'PackageFee', 'rules' => 'required|min_length[1]|max_length[25]'),
//    array('field' => 'CorporationShare', 'label' => 'CorporationShare', 'rules' => 'required|min_length[3]|max_length[25]'),
//    array('field' => 'HideShareValue', 'label' => 'HideShareValue', 'rules' => 'required|min_length[1]|max_length[5]'),
//    array('field' => 'Expedite', 'label' => 'Expedite', 'rules' => 'required|min_length[1]|max_length[5]')


);