<?php
/**
 * Created by PhpStorm.
 * User: DELL OPTIPLEX 380
 * Date: 4/16/2015
 * Time: 10:57 AM
 */

require_once('stripe-php-2.1.4/vendor/autoload.php');
class State_model extends CI_Model
{
    public $a=0;

///////////////////////////// Register ////////////////////////////////////////////
    public  function register()
    {
        $UserName = $this->input->post('UserName');
        $Name = $this->input->post('Name');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');


        $query = $this->db->get('register');
        foreach($query->result() as $data)
        {
            if ($data->UserName == $UserName || $data->Email== $Email )
            {
                error("This UserName or Email is already exist");
                die();

            }



        }
        $data1 = array(

            'UserName' => $UserName,
            'Name' => $Name,
            'Email' => $Email,
            'Password' => $Password

        );
        $a = $this->db->insert('register', $data1);
        if ($a == 1)
        {
            $id= $this->getRegisterId($UserName,$Password);


            register("Add user Successfully!",$id);

        }
        else {
            error("error");
        }

    }


   ///////////////////////////////////////////////////////////////////////////////////////////////////////



    /////////////////////////////////////// LOGIN ////////////////////////////////////////////////////////////////
    public function login()
    {
        $username=$this->input->post('UserName');
        $password=$this->input->post('Password');
        $query = $this->db->get('register');
        if ($query->num_rows > 0)
        {
            foreach($query->result() as $data)
            {
                if (($data->UserName == $username ||$data->Email==$username) && $data->Password == $password)
                {
                    $rId = $data->registerId;

                    $this->companyStatus($rId);

                    success1("login successfully",$rId);
                    break;
                }


            }
            error("Invaliid login");

        }
        else
        {
            error('error');
        }
    }
    public function companyStatus($id)

    {
        $this->db->select('b.CompanyFName,b.Date,b.CompanyId,s.statusName');
        $this->db->from('businessinfo b,status s');
        $this->db->where('b.StatusId = s.StatusId  ');
        $this->db->where('b.registerID',$id);
        $query = $this->db->get();

        if ($query->num_rows >0 )
        {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            $token=$this->stripe($id);
            success2("status",$token,$id,$a);


        }
        else
        {
            $a=array();
            success('you did not submit any application ',$a);
        }







    }
    public function stripe($id)
    {
        $query=$this->db->get('tokenNbillingAddress');
        if($query->num_rows>0)
        {
            foreach($query->result() as $row)
            {
                if($row->registerId==$id)
                {
                    $a=$row->stripeToken;
                }
            }
            return $a;
        }

    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////// ADD APPLICATION /////////////////////////////////////////////////////////////
    public function addAllInfo()
    {
       $pkgfees= $this->input->post('PackageFee');
        $rId = $this->input->post('registerId');




        $true=$this->stripePaymentOperation($pkgfees,$rId);
        if($true==true)
        {

            $dt = new DateTime();
            $date = $dt->format('Y-m-d');

            $state = $this->input->post('State');
            $name = $this->input->post('CompanyFName');
            $lname = $this->input->post('CompanyLName');
            $street = $this->input->post('Street');
            $city = $this->input->post('City');
            $zipcode = $this->input->post('ZipCode');
            $country = $this->input->post('Country');
            $comSummary = $this->input->post('CompanySummary');
            $rId = $this->input->post('registerId');
            $data1 = array(
                'State' => $state,
                'CompanyFName' => $name,
                'CompanyLName' => $lname,
                'Street' => $street,
                'City' => $city,
                'ZipCode' => $zipcode,
                'Country' => $country,
                'CompanySummary' => $comSummary,
                'StatusId' => 0,
                'registerID' => $rId,
                'payment' => 1,
                'Date' => $date


            );
            $a = $this->db->insert('businessinfo', $data1);

            $query1 = $this->db->query("SELECT CompanyId FROM businessinfo order by CompanyId desc limit 1");
            if ($query1->num_rows > 0) {
                foreach ($query1->result() as $row)
                {
                    $cid = $row->CompanyId;

                }


            }
            $mInfo = $this->input->post('MemberInfo');
            $data2 = json_decode($mInfo);

            foreach ($data2 as $row)
            {
                $test = array(
                    'PostName' => $row->PostName,
                    'Name' => $row->Name,
                    'Address' => $row->Address,
                    'Street' => $row->Street,
                    'City' => $row->City,
                    'State' => $row->State,
                    'ZipCode' => $row->ZipCode,
                    'Country' => $row->Country,
                    'CId' => $cid);
                $b = $this->db->insert('membersinfo', $test);


            }
            $bcname = $this->input->post('BusinessCategoryName');
            $selectpkg = $this->input->post('SelectPackage');
            $pkgfees = $this->input->post('PackageFee');
            $corpshare = $this->input->post('CorporationShare');
            $hidevalue = $this->input->post('HideShareValue');
            $Managed = $this->input->post('Managed');


            $data3 = array(
                'BusinessCategoryName' => $bcname,
                'Managed' => $Managed,
                'CorporationShare' => $corpshare,
                'HideShareValue' => $hidevalue,
                'SelectPackage' => $selectpkg,
                'PackageFee' => $pkgfees,
                'CId' => $cid
            );
            $c = $this->db->insert('businesscategory', $data3);


            $Sstreet = $this->input->post('ShippingStreet');
            $Scity = $this->input->post('ShippingCity');
            $Szipcode = $this->input->post('ShippingZipCode');
            $Scountry = $this->input->post('ShippingCountry');
            $Stelephone = $this->input->post('Telephone');
            $Semail = $this->input->post('Email');
            $Sstate = $this->input->post('ShippingState');
           // $rId = $this->input->post('registerId');
            $data1 = array(
                'Street' => $Sstreet,
                'City' => $Scity,
                'ZipCode' => $Szipcode,
                'Telephone' => $Stelephone,
                'Country' => $Scountry,
                'Email' => $Semail,
                'State' => $Sstate,
                'CId' => $cid,
                'RegisterId' => $rId


            );
            $d = $this->db->insert('companyShippingAddress', $data1);


            $e = $this->testing($rId);


            if ($a == 1 && $b && $c == 1 && $d == 1)
            {
                $data = array(
                    'companyId' => $cid,

                );

                $this->db->where('registerId', $rId);
                $this->db->order_by('paymentInfoId', 'DESC');
                $this->db->limit('1');
                $this->db->update('paymentInfo', $data);

                successRecord("Add all Information Successfully ", $e);

            } else {
                error("error");
            }

        }
        else
        {
            error("Application not saved..");
        }

    }
    //////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////// FORGET PASSWORD .///////////////////////////////////////////////


    public function forgetPassword()
    {
        $this->load->library('email');
        $email=$this->input->post('Email');
        $this->db->select('*');
        $this->db->from('register');
        $this->db->where('Email',$email);
        $query=$this->db->get();
        if($query->num_rows() > 0)
        {
            foreach($query->result() as $row)
            {
                if($row->Email == $email )
                {


                    $pass=$row->Password;
                    $password='Your password is :  '.$pass;

                    $this->email->from('info@o16-labs.com');
                    $this->email->to($email);

                    $this->email->subject('Password');
                    $this->email->message($password);
                    if($this->email->send())
                    {
                        success1('Valid Email');

                    }
                    else
                    {
                        error('Invalid Email ');
                    }

                }
            }
            error('Invalid Email ');
        }
        else
        {
            error('Invalid Email ');
        }
    }
















    ///////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////// Stripetoken info ////////////////////////////////////////
    public function payment()
    {
        \Stripe\Stripe::setApiKey("sk_test_JUeMD9DeOVXiikNQJHvoRJih");
        $token=$this->input->post('stripeToken');
        $rId = $this->input->post('registerId');
        //  Create the charge on Stripe's servers - this will charge the user's card
        try {
            $customer = \Stripe\Customer::create(array(
                    "source" => $token,
                    "description" => "Example customer")
            );

// Charge the Customer instead of the card
//            \Stripe\Charge::create(array(
//                    "amount" => 1000, # amount in cents, again
//                    "currency" => "gbp",
//                    "customer" => $customer->id)
//            );
        }
        catch(\Stripe\Error\Card $e)
        {
            // The card has been declined
        }
        $b1 = $this->input->post('billingA1');
        $b2 = $this->input->post('billingA2');
        $bcity = $this->input->post('bCity');
        $bstate = $this->input->post('bState');
        $bzipcode = $this->input->post('bZipCode');
        $chname = $this->input->post('CardHolderName');

        $data1 = array(


            'billingA1' => $b1,
            'billingA2' => $b2,
            'bCity' => $bcity,
            'bState' => $bstate,
            'bZipCode' => $bzipcode,
            'stripeToken' => $token,
            'CardHolderName'=>$chname,
            'registerId' => $rId,
            'CustomerId'=>$customer->id


        );

        $b=$this->db->insert('tokenNbillingAddress', $data1);

        if ($b==1) {
            success1("Save Token and Billing information saved successfuly");

        } else {
            error("Save Token and Billing information is not saved successfuly");
        }

    }


///////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////Payment operation//////////////////////////////////////////////////////////


    public function stripePaymentOperation($fees,$rId)
    {
        //$fees= $this->input->post('PackageFee');
        //$rId = $this->input->post('registerId');

        \Stripe\Stripe::setApiKey("sk_test_JUeMD9DeOVXiikNQJHvoRJih");
        $fees=$fees *100;



        $query2 = $this->db->get('tokenNbillingAddress');
        foreach ($query2->result() as $row)
        {
            if ($row->registerId == $rId)
            {
                $cid = $row->CustomerId;
                $token = $row->stripeToken;
                try
                {

                    $charge = \Stripe\Charge::create(array(
                            "amount" => $fees, # amount in cents, again
                            "currency" => "usd",

                            "customer" => $cid)
                    );

                } catch (\Stripe\Error\Card $e)
                {
                    echo" The card has been declined";
                    echo "Error ".$e;
                }


                $data1 = array(


                    'stripeToken' => $token,
                    'customerId' => $cid,
                    'registerId' => $rId,
                    'PackageFees' => $fees,
                    'companyId'=>null


                );

                $a = $this->db->insert('paymentInfo', $data1);
                if ($a == 1)
                {

                    return true;



                    // success1("Payment operation done successfully ");
                }



            }

        }


    }
//        try {
////            $customer = \Stripe\Customer::create(array(
////                    "source" => $token,
////                    "description" => "Example customer")
////            );
//
//// Charge the Customer instead of the card
//            $charge=\Stripe\Charge::create(array(
//                    "amount" => $fees, # amount in cents, again
//                    "currency" => "usd",
//
//                    "customer" => $cid)
//            );
//
//        }
//
//        catch(\Stripe\Error\Card $e) {
//            // The card has been declined
//        }
//      $customerId= $charge->customer;
//
//
//
//
//        $data1 = array(
//
//
//            'stripeToken'=>$token,
//            'customerId'=>$customerId,
//            'registerId'=>$rId,
//            'PackageFees'=>$fees,
//
//
//
//        );
//        $a=$this->db->insert('paymentInfo',$data1);
//        if($a==1)
//        {
//            return true;
//
//           // success1("Payment operation done successfully ");
//        }
//        else
//        {
//            error('Payment operation is not done successfully ');
//        }
//








////////////////////////////////////////////////////////////////////////////////////////////////////
######################################################################################################################
                      //      CREATE BUSINESS ACCOUNT (1)
######################################################################################################################

    public function createBusinessAccount()
    {
        $state = $this->input->post('State');
        $name = $this->input->post('CompanyFName');
        $lname = $this->input->post('CompanyLName');
        $street= $this->input->post('Street');
        $city = $this->input->post('City');
        $zipcode = $this->input->post('ZipCode');
        $country = $this->input->post('Country');
        $comSummary = $this->input->post('CompanySummary');
        $data1 = array(
            'State' => $state,
            'CompanyFName' => $name,
            'CompanyLName' => $lname,
            'Street' => $street,
            'City' => $city,
            'ZipCode' => $zipcode,
            'Country' => $country,
            'CompanySummary' => $comSummary

        );
        $a=$this->db->insert('businessinfo',$data1);

        if ($a== 1 )
        {
                success1("Account Created Successfully");
            }
            else
            {
                error("Error");
            }



    }
######################################################################################################################
                         //     ADD COMPANY MEMBER (2)
######################################################################################################################

    public function addCompanyMembers()
    {
        $mPname = $this->input->post('PostName');
        $mName = $this->input->post('Name');
        $mStreet = $this->input->post('Street');
        $mCity = $this->input->post('City');
        $mState = $this->input->post('State');
        $mZipcode = $this->input->post('ZipCode');
        $mCountry = $this->input->post('Country');
        $mAddress = $this->input->post('Address');
        $CId = $this->input->post('CId');
        $data1 = array(

            'PostName' => $mPname,
            'Name' => $mName,
            'Street' => $mStreet,
            'City' => $mCity,
            'State' => $mState,
            'ZipCode' => $mZipcode,
            'Country' => $mCountry,
            'Address' => $mAddress,
            'CId' => $CId

        );
        $this->db->insert('membersinfo', $data1);
        $a = $this->db->affected_rows();
        if ($a == 1) {
            success1("Add MembersInfo Successfully!");

        } else {
            error("error");
        }


    }
 ######################################################################################################################
                    //     ADD BUSINESS CATEGORY (3)
######################################################################################################################

    public function addBusinessCategory()
    {
        $bcname = $this->input->post('BusinessCategoryName');
        $selectpkg = $this->input->post('SelectPackage');
        $pkgfees= $this->input->post('PackageFee');
        $corpshare = $this->input->post('CorporationShare');
        $hidevalue= $this->input->post('HideShareValue');
        //$Expedite= $this->input->post('Expedite');
        $CId=$this->input->post('CId');

        $data3 = array(
            'BusinessCategoryName' => $bcname,
            'SelectPackage' => $selectpkg,
            'CorporationShare' => $corpshare,
            'HideShareValue' => $hidevalue,
            'PackageFee' => $pkgfees,
           // 'Expedite' => $Expedite,
            'CId'=>$CId
        );
        $this->db->insert('businesscategory',$data3);
        $c= $this->db->affected_rows();
        if ($c==1)
        {
            success1("Add Business Category Info Successfully!");

        } else {
            error("error");
        }



    }
######################################################################################################################
                          //   GET ALL DETAIL OF STATE  (4)
######################################################################################################################

    public function getDetail()
    {
        $query = $this->db->get('stateinfo');

        // CHECK RETURN RESULTS

        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $responseData[] = $row;
            }
            success("stateInfo", $responseData);
        } else {
            error("Error");
        }
    }

######################################################################################################################
                       //   ADD COMPANY BUSINESS , MEMBERS AND CATEGORY INFORMATION (5)
######################################################################################################################

    public  function addInfo()
    {
//        $state = $this->input->post('State');
//        $name = $this->input->post('CompanyFName');
//        $lname = $this->input->post('CompanyLName');
//        $street= $this->input->post('Street');
//        $city = $this->input->post('City');
//        $zipcode = $this->input->post('ZipCode');
//        $country = $this->input->post('Country');
//        $comSummary = $this->input->post('CompanySummary');
//        $data1 = array(
//            'State' => $state,
//            'CompanyFName' => $name,
//            'CompanyLName' => $lname,
//            'Street' => $street,
//            'City' => $city,
//            'ZipCode' => $zipcode,
//            'Country' => $country,
//            'CompanySummary' => $comSummary
//
//        );
//        $this->db->insert('businessinfo',$data1);
//        $a = $this->db->affected_rows();

######################################################################################################################
        // ADD COMPANY MEMBERS INFORMATION (4)
######################################################################################################################

        //  $mPname = $this->input->post('PostName');
        // $mName = $this->input->post('mName');
        // $mStreet= $this->input->post('mStreet');
        //  $mCity = $this->input->post('mCity');
        // $mState = $this->input->post('mState');
        // $mZipcode = $this->input->post('mZipCode');
        // $mCountry = $this->input->post('mCountry');
        //   $mAddress = $this->input->post('mAddress');

        //        $bcname = $this->input->post('BusinessCategoryName');
//        $selectpkg = $this->input->post('SelectPackage');
//        $pkgfees= $this->input->post('PackageFee');
//        $corpshare = $this->input->post('CorporationShare');
//        $hidevalue= $this->input->post('HideShareValue');
//        $Expedite= $this->input->post('Expedite');
//
//        $data3 = array(
//            'BusinessCategoryName' => $bcname,
//            'SelectPackage' => $selectpkg,
//            'CorporationShare' => $corpshare,
//            'HideShareValue' => $hidevalue,
//            'PackageFee' => $pkgfees,
//            'Expedite' => $Expedite
//        );
//        $this->db->insert('businesscategory',$data3);
//        $c= $this->db->affected_rows();


        $bInfo= $this->input->post('businessinfo');
        $data1=json_decode($bInfo);

        foreach($data1 as $row)
        {
            $this->db->insert('businessinfo', $row);
            $a = $this->db->affected_rows();
        }

        $mInfo= $this->input->post('MemberInfo');
        $data2=json_decode($mInfo);

        foreach($data2 as $row)
        {
            $this->db->insert('membersinfo', $row);
            $b = $this->db->affected_rows();
        }

        $bcInfo= $this->input->post('businesscategory');
        $data3=json_decode($bcInfo);

        foreach($data3 as $row)
        {
            $this->db->insert('businesscategory', $row);
            $c = $this->db->affected_rows();
        }



        if ($a== 1 && $b==1 && $c==1)
        {
            success1("Add Info Successfully");

        } else {
            error("error");
        }

    }
######################################################################################################################
                        // GET COMPANY BUSINESS INFORMATION (6)
######################################################################################################################

    public function getBusinessInfo()
    {
        $query = $this->db->get('businessinfo');
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $responseData[] = $row;
            }
            success("BusinessInfo", $responseData);
        } else
        {
            error("Error");
        }
    }
######################################################################################################################
                                  // GET COMPANY MEMBERS INFORMATION (7)
######################################################################################################################

    public function getCompanyMember()
    {
        $query = $this->db->get('membersinfo');
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $responseData[] = $row;
            }
            success("MembersInfo", $responseData);
        } else {
            error("Error");
        }
    }

######################################################################################################################
                  // GET BUSINESS CATEGORY AND PACKAGE (8)
######################################################################################################################

    public  function getBusinessCategory()
    {
        $query = $this->db->get('businesscategory');
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $responseData[] = $row;
            }
            success("BusinessCategoryInfo", $responseData);
        }
        else
        {

            error("Error");
        }
    }
######################################################################################################################
                // GET EXPEDITE INFO (8)
######################################################################################################################

       public function stateFeesInfo()
       {
           $state = $this->input->post('State');
           $CompanyType = $this->input->post('CompanyType');
           $this->db->select('CorpFee,LLCFee,SuperBizCorpFee,SuperBizLLCFee,Expedite,Fee,TimeExpedited');
           $this->db->from('stateinfo');
           $this->db->where('State',$state);
           $query = $this->db->get();
           if ($query->num_rows > 0) {
               foreach ($query->result() as $row)
               {
                   if($CompanyType=='LLC')
                   {
                       $a=$row->LLCFee;
                   }
                   else
                   {
                       $a=$row->CorpFee;
                   }
                  // $a=$row;
               }
               success('LLC and Corporation Fees Information',$a);



           }
           error('No Record Found');


        }


######################################################################################################################
                 // RETURN INFO AND DISPLAY IN HOME AND HOME DETAIL PAGE(8)
######################################################################################################################

   public function showdetail()
    {

      //$this->db->select('bussinessinfo.State','businesscategory.Expedite');
        $this->db->select('*');
        $this->db->from('businessinfo');
        $this->db->join('businesscategory','businessinfo.CompanyId=businesscategory.BusinessCategoryId');

        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            //success("detail",$a);

            return $a;

        }


    }
######################################################################################################################
    // RETURN INFO AND DISPLAY IN HOME DETAIL PAGE(9)
######################################################################################################################

    public function memberdetail()
    {
        $this->db->select('*');
        $this->db->from('membersinfo');
        $this->db->join('businessinfo','membersinfo.CId=businessinfo.CompanyId');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            //success("detail",$a);

            return $a;

        }
    }


######################################################################################################################
               // CHECK VALID ADMIN LOGIN (10)
######################################################################################################################


     public  function attemptLogin($username,$password)
     {
         $query = $this->db->get('login');
         if ($query->num_rows > 0)
         {
             foreach($query->result() as $data) {
                 if ($data->UserName == $username && $data->Password == $password) {


                     return true;
                 }
             }
             return false;

         }
     }





    public function test()
    {
        $test=array(
           array('PostName'=>'pname',
                'Name' => 'name',
                'Address'=>'address',
                'Street' => 'street',
                'City' => 'city',
                'State' => 'state',
                'ZipCode' => 'zipcode',
                'Country' => 'country',
                'BusinessId'=>1),
            array('PostName'=>'pname',
            'Name' => 'name',
            'Address'=>'address',
            'Street' => 'street',
            'City' => 'city',
            'State' => 'state',
            'ZipCode' => 'zipcode',
            'Country' => 'country',
                 'BusinessId'=>1
        ));

           echo json_encode($test);
    }
    public function detail()
    {
        $this->db->select('*');
        $this->db->from('businessinfo');
        $this->db->join('businesscategory','businessinfo.CompanyId=businesscategory.BusinessCategoryId');
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            //success("detail",$a);

            return $a;

        }



    }######################################################################################################################
    //  (7)
######################################################################################################################

public function showdetail1($limit, $start)// FOR ALL dETAILS
{

//   $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
    $this->db->select('a.CompanyId,a.State,a.CompanyFName,b.Expedite,a.StatusId,a.payment');
    $this->db->from('businessinfo a, stateinfo b');
    $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
    $this->db->limit($limit, $start);


    $query = $this->db->get();

    if ($query->num_rows > 0)
    {
        foreach ($query->result() as $row)
        {
            $a[]=$row;
        }
        //success("detail",$a);

        return $a;

    }
    return false;



}


    public function showdetail_notsubmit($limit, $start)// FOR ALL dETAILS---> c1() FOR TOTal count
    {

//   $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('a.CompanyId,a.State,a.CompanyFName,b.Expedite,a.StatusId,a.payment');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',0);
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
        $this->db->limit($limit, $start);


        $query = $this->db->get();

        if ($query->num_rows > 0)
        {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            //success("detail",$a);

            return $a;

        }
        return false;



    }

    public function showdetail_submit($limit, $start)// FOR ALL dETAILS---> c1() FOR TOTal count
    {

//   $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('a.CompanyId,a.State,a.CompanyFName,b.Expedite,a.StatusId,a.payment');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',1);
        $this->db->where('a.State = b.State ');
        $this->db->limit($limit, $start);


        $query = $this->db->get();

        if ($query->num_rows > 0)
        {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            //success("detail",$a);

            return $a;

        }
        return false;



    }
    public function showdetail_process($limit, $start)// FOR ALL dETAILS---> c1() FOR TOTal count
    {

//   $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('a.CompanyId,a.State,a.CompanyFName,b.Expedite,a.StatusId,a.payment');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',2);
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
        $this->db->limit($limit, $start);


        $query = $this->db->get();

        if ($query->num_rows > 0 )
        {
            foreach ($query->result() as $row)
            {
                $a[]=$row;
            }
            //success("detail",$a);

            return $a;

        }
        return false;



    }





    public function showdetail2($limit, $start)
    {
      $id= $this->uri->segment(3);

        $this->db->select('a.CompanyId,a.State,a.CompanyFName,b.Expedite,a.StatusId');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',$id);
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');

        $this->db->limit($limit, $start);


        $query = $this->db->get();

        if ($query->num_rows > 0)
        {
            foreach ($query->result() as $row)
            {


                {
                    $a[] = $row;
                }

            }

            //success("detail",$a);

            return $a;

        }
        return false;

    }



    public function Expedite()
    {
        $state=$this->input->post('State');
        $this->db->select('Expedite,Fee,TimeExpedited');
        $this->db->from('stateinfo');
        $this->db->where('State',$state);
        $query=$this->db->get();
        if($query->num_rows>0)
        {
            foreach($query->result() as $row)
            {
                $a=$row;
            }
            success('expidateInfo',$a);
        }


//        $query2=$this->db->query("SELECT CompanyId FROM businessinfo order by CompanyId desc limit 1");
//        if ($query2->num_rows > 0)
//        {
//            foreach ($query2->result() as $row)
//            {
//                $cid=$row->CompanyId;
//
//            }
//
//
//        }
//
//
//            $query = $this->db->query("select a.State,a.CompanyId, b.SelectPackage, b.PackageFee,c.Expedite, c.Fee, c.TimeExpedited from
//                                            businessinfo a, businesscategory b, stateinfo c
//                                               where a.CompanyId=b.CId and a.State=c.State and c.Expedite='Yes';");
//
//            $query1 = $this->db->query("select a.State,a.CompanyId, b.SelectPackage, b.PackageFee,c.Expedite from
//                                                businessinfo a, businesscategory b, stateinfo c
//                                                    where a.CompanyId=b.CId and a.State=c.State and c.Expedite='No';");
//
//
//            //    $query1=$this->db->query("select a.State, b.SelectPackage, b.PackageFee from businessinfo a
//            //                          INNER JOIN businesscategory b ON a.CompanyId=b.CId
//            //                              INNER JOIN  stateinfo c ON a.State=c.State
//            //                                  WHERE b.Expedite='no';");
//
//            if ($query->num_rows > 0) {
//                foreach ($query->result() as $row) {
//                    if ($row->CompanyId == $cid) {
//                        $responseData = $row;
//                    }
//                }
//
//            }
//
//           if($query1->num_rows > 0)
//                {
//                foreach ($query1->result() as $row1) {
//                    if($row1->CompanyId==$cid)
//                    {
//                        $responseData = $row1;
//                    }
//
//                   }
//                    if(!$responseData==false) {
//
//                        success("ExpidateInfo", $responseData);
//                    }
//                    else {
//                        error("Error");
//                    }
//            }
//            else {
//                error("Error");
//            }
        }

    public function record_count()
    {



     //return $this->db->count_all("businessinfo");
        return $this->db->count_all("businessinfo");

//       return $query1;

    }
    public function count1()
    {

       $id= $this->uri->segment(3);
        $this->db->select('count(StatusId)');
       $this->db->from('businessinfo');
       $this->db->where('StatusId', $id);
        $count = $this->db->count_all_results();
        return $count;






    }


    public function c()  //count all record of homepage
    {
       // $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('count(a.CompanyId)');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
        $count = $this->db->count_all_results();
        return $count;
    }

    public function c1()//FOR NOT SUBMIT
    {
        // $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('count(a.CompanyId)');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',0);
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
        $count = $this->db->count_all_results();
        return $count;
    }
    public function c2()//FOR  SUBMIT
    {
        // $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('count(a.CompanyId)');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',1);
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
        $count = $this->db->count_all_results();
        return $count;
    }
    public function c3()//FOR PROCESS COMPLETE
    {
        // $query=$this->db->query("select CompanyId,State,CompanyFName from  businessinfo;");
        $this->db->select('count(a.CompanyId)');
        $this->db->from('businessinfo a, stateinfo b');
        $this->db->where('a.StatusId',2);
        $this->db->where('a.State = b.State ORDER BY a.CompanyId ASC ');
        $count = $this->db->count_all_results();
        return $count;
    }
    public function register1()
    {
        $UserName = $this->input->post('UserName');
        $Email = $this->input->post('Email');
        $Password = $this->input->post('Password');


        $data1 = array(

            'UserName' => $UserName,
            'Email' => $Email,
            'Password' => $Password

        );
        $this->db->insert('register', $data1);
        $a = $this->db->affected_rows();
        if ($a == 1) {
            success1("Account created  Successfully!");

        } else {
            error("error");
        }





    }

    public function getRegisterId($username,$password)
    {
        $query = $this->db->get('register');
        foreach($query->result() as $data)
        {
            if (($data->UserName == $username||$data->Email==$username) && $data->Password == $password)
            {
                $rId = $data->registerId;
                return $rId;

            }



        }


    }







    public function stripePayment()
    {
        \Stripe\Stripe::setApiKey("sk_test_JUeMD9DeOVXiikNQJHvoRJih");

        $token=$this->input->post('stripeToken');

       //  Create the charge on Stripe's servers - this will charge the user's card
        try {
            $customer = \Stripe\Customer::create(array(
                    "source" => $token,
                    "description" => "Example customer")
            );

// Charge the Customer instead of the card
//            \Stripe\Charge::create(array(
//                    "amount" => 1000, # amount in cents, again
//                    "currency" => "gbp",
//                    "customer" => $customer->id)
//            );
        }
        catch(\Stripe\Error\Card $e) {
            // The card has been declined
        }
       // $cid=$customer->id;

//        $UserName = $this->input->post('UserName');
//
//        $Password = $this->input->post('Password');
//        $query = $this->db->get('register');
//        foreach($query->result() as $data)
//        {
//            if (($data->UserName == $UserName||$data->Email==$UserName) && $data->Password == $Password)
//            {
//                $rId = $data->registerId;
//
//            }
//
//
//
//        }

        $rId = $this->input->post('registerId');

        $chName = $this->input->post('cardholderName');
        $cName = $this->input->post('cardNumber');
        $token=$this->input->post('stripeToken');
       // $cid=$customer->id;
       // $rId=$row->registerId;

        $data1 = array(

            'cardholderName' => $chName,
            'stripeToken'=>$token,
           // 'customerId'=>$cid,
            'registerId'=>$rId


        );
        $b=$this->db->insert('stripePayment', $data1);

        if ($b==1) {
            success1("Stripe Payment information saved Successfully!");

        } else {
            error("Stripe Payment information is not save Successfully!");
        }

    }
      public function chAddress()
      {



        $b1 = $this->input->post('billingA1');
        $b2 = $this->input->post('billingA2');
        $bcity = $this->input->post('bCity');
        $bstate = $this->input->post('bState');
          $bzipcode = $this->input->post('bZipCode');
        $s1 = $this->input->post('shippingA1');
        $s2 = $this->input->post('shippingA2');
        $scity = $this->input->post('sCity');
        $sstate = $this->input->post('sState');
          $rId = $this->input->post('registerId');

          $data1 = array(


              'billingA1' => $b1,
              'billingA2' => $b2,
              'bCity' => $bcity,
              'bState' => $bstate,
              'bZipCode' => $bzipcode,
              'shippingA1' => $s1,
              'shippingA2' => $s2,
              'sCity' => $scity,
              'sState' => $sstate,
              'registerId' => $rId


          );

          $b=$this->db->insert('chAddress', $data1);

          if ($b==1) {
              success1("Add card holder Addresses information Successfully!");

          } else {
              error("Add card holder Addresses information is not  Successfully!");
          }
        //  $UserName = $this->input->post('UserName');

         // $Password = $this->input->post('Password');
         // $query = $this->db->get('register');
         // if ($query->num_rows > 0)
         // {
//              foreach ($query->result() as $data) {
//                  if (($data->UserName == $UserName || $data->Email == $UserName) && $data->Password == $Password) {
//                      $rId = $data->registerId;
//
//                  }
//
//
//              }

       //   }





      }

    public function applicationInfo()
         {
             $id=$this->input->post('CompanyId');
    $this->db->select('b.CompanyFName,b.Street,b.City,b.ZipCode,b.State,b.Country,b.CompanySummary,b.payment,c.BusinessCategoryName,c.SelectPackage,c.PackageFee');
    $this->db->from('businessinfo b,businesscategory c');

    $this->db->where('b.CompanyId ',$id);
    $this->db->where('c.CId ',$id);
    $query = $this->db->get();

    if ($query->num_rows >0 )
    {
        foreach ($query->result() as $row)
        {
//           $a=$row->CompanyName;
//            $b=$row->Street;
//            $c=$row->City;
//            $d=$row->State;
//            $e=$row->Country;
//            $f=$row->CompanySummary;
//            $g=$row->BusinessCategoryName;
//            $h=$row->SelectPackage;
//            $i=$row->PackageFee;
//            $j=$row->payment;
//            $message="'ApplicationInfo";
            $a=$row;

        }
    success('application info',$a);

    }


             else
             {
                 error('Data Not Found');
             }

         }




public function updateToken()
{
    \Stripe\Stripe::setApiKey("sk_test_JUeMD9DeOVXiikNQJHvoRJih");
    $token=$this->input->post('stripeToken');

    try {
        $customer = \Stripe\Customer::create(array(
                "source" => $token,
                "description" => "Example customer")
        );


    } catch (\Stripe\Error\Card $e) {
        echo "The card has been declined";
        // The card has been declined
    }

    $UserName = $this->input->post('UserName');

    $Password = $this->input->post('Password');
    $query = $this->db->get('register');
    foreach($query->result() as $data)
    {
        if (($data->UserName == $UserName||$data->Email==$UserName) && $data->Password == $Password)
        {
            $rId = $data->registerId;

        }



    }

    $data = array(
        'stripeToken' => $token

    );

    $this->db->where('registerId',$rId);
    $this->db->update('stripePayment',$data);
    success1("successfully Update");

}


    public function setID($rid)
    {
        $this->a=$rid;

    }
    public function getId()
    {
       return $this->a;



    }
    public function companySA()
    {
//        $UserName = $this->input->post('UserName');
//
//        $Password = $this->input->post('Password');
//        $query = $this->db->get('register');
//        foreach($query->result() as $data)
//        {
//            if (($data->UserName == $UserName||$data->Email==$UserName) && $data->Password == $Password)
//            {
//                $rId = $data->registerId;
//
//            }
//
//
//
//        }
        $query1=$this->db->query("SELECT CompanyId FROM businessinfo order by CompanyId desc limit 1");
        if ($query1->num_rows > 0)
        {
            foreach ($query1->result() as $row)
            {
                $cid=$row->CompanyId;

            }


        }
        $street= $this->input->post('Street');
        $city = $this->input->post('City');
        $zipcode = $this->input->post('ZipCode');
        $country = $this->input->post('Country');
        $telephone = $this->input->post('Telephone');
        $email = $this->input->post('Email');
        $state = $this->input->post('State');
        $rId = $this->input->post('registerId');
        $data1 = array(
            'Street' => $street,
            'City' => $city,
            'ZipCode' => $zipcode,
            'Telephone' => $telephone,
            'Country' => $country,
            'Email' => $email,
            'State'=>$state,
            'CId'=>$cid,
            'RegisterId'=>$rId




        );
        $a=$this->db->insert('companyShippingAddress',$data1);
        if ($a==1) {
            success1("Add Company Shipping Address information Successfully!");

        } else {
            error("error");
        }

    }


        public function stripePaymentPage()
        {
            //get register id from login user
         //   $UserName = $this->input->post('UserName');
         //   $Password = $this->input->post('Password');
           // $query = $this->db->get('register');
           // foreach($query->result() as $data)
          //  {
             //   if (($data->UserName == $UserName || $data->Email == $UserName) && $data->Password == $Password)
              //  {
                 //   $rId = $data->registerId;
                    ////////////////////////////////////////
                    $rId = $this->input->post('registerId');
                    $b=$this->testing($rId);

                    /////////////////////////////////////////////
                    $this->db->select('SP.cardholderName,ch.billingA1,ch.bCity,ch.bZipCode');
                    $this->db->from('stripePayment SP,chAddress ch');
                    $this->db->where('SP.registerId',$rId);
                    $this->db->where('ch.registerID',$rId);
                    $query1 = $this->db->get();
                    if ($query1->num_rows >0 )
                    {
                        foreach ($query1->result() as $row)
                        {
                            $a=$row;
                        }
                        successRecord("Stripe Info",$a,$b);
                        die();


                    }
                    else
                    {
                        error('No Data Found');
                        die();
                    }








}
    public function testing($rId)
{


    $this->db->select('b.CompanyFName,b.Date,b.CompanyId,s.statusName');
    $this->db->from('businessinfo b,status s');
    $this->db->where('b.StatusId = s.StatusId  ');
    $this->db->where('b.registerID',$rId);
   // $this->db->order_by('b.CompanyId', 'desc');
    $this->db->order_by('b.CompanyId', 'DESC');
    $this->db->limit('1');
    $query = $this->db->get();

    if ($query->num_rows >0 )
    {
        foreach ($query->result() as $row)
        {
            $a=$row;
        }
        return $a;


    }










//    $query1 = $this->db->query1("select b.CompanyFName,b.Date,b.CompanyId,s.statusName from
//                                           businessinfo b,status s
//                                               where b.StatusId = s.StatusId  and b.registerID=$rId ORDER by b.CompanyId desc limit 1;");
//
//
//
//
//
//    if ($query1->num_rows >0 )
//    {
//        foreach ($query1->result() as $row)
//        {
//            $b[]=$row;
//
//
//        }
//        echo var_dump($b);
//        die();
//
//
//
//       return $b;
//    }


}

    public function stripePaymentOperation1()
    {


        \Stripe\Stripe::setApiKey("sk_test_JUeMD9DeOVXiikNQJHvoRJih");
        $rId = $this->input->post('registerId');
//        $UserName = $this->input->post('UserName');
//        $Password = $this->input->post('Password');
//        $query = $this->db->get('register');
//        foreach($query->result() as $data) {
//            if (($data->UserName == $UserName || $data->Email == $UserName) && $data->Password == $Password) {
//                $rId = $data->registerId;
//
//            }
//        }

                  $query2=$this->db->query("SELECT CompanyId FROM businessinfo order by CompanyId desc limit 1");
                  if ($query2->num_rows > 0)
                  {
                  foreach ($query2->result() as $row)
                    {
                     $cId=$row->CompanyId;

                    }


                  }
                  $query1=$this->db->get('businesscategory');
                  foreach($query1->result() as $data)
                  {
                      if ($data->CId == $cId) {
                          $fees = $data->PackageFee;
                      }


                  }
        $query2=$this->db->get('tokenNbillingAddress');
        foreach($query2->result() as $row)
        {
            if($row->registerId ==$rId)
            {
                $token=$row->stripeToken;
            }
        }


        //  Create the charge on Stripe's servers - this will charge the user's card
        try {
            $customer = \Stripe\Customer::create(array(
                    "source" => $token,
                    "description" => "Example customer")
            );

            // Charge the Customer instead of the card
            \Stripe\Charge::create(array(
                    "amount" => $fees, # amount in cents, again
                    "currency" => "gbp",
                    "customer" => $customer->id)
            );
        } catch (\Stripe\Error\Card $e) {
            echo "The card has been declined";
            // The card has been declined
        }
       $cid=$customer->id;
        $data1 = array(


            'stripeToken'=>$token,
            'customerId'=>$cid,
            'registerId'=>$rId,
            'PackageFees'=>$fees,
            'CId'=>$cId


        );
        $a=$this->db->insert('paymentInfo',$data1);
        if($a==1)
        {
            $data = array(
                'payment' => 1,

            );

            $this->db->where('CompanyId', $cId);
            $this->db->update('businessinfo', $data);
         success1("Payment operation done successfully ");
        }
        else
        {
            error('Payment operation is not done successfully ');
        }



    }
    public function getAllStates()
    {
      //  SuperBizCorpFee,SuperBizLLCFee,
      //  $state = $this->input->post('State');
       // $CompanyType = $this->input->post('CompanyType');
        $this->db->select('State,SuperBizCorpFee,SuperBizLLCFee,Expedite,Fee,TimeExpedited');
        $this->db->from('stateinfo');
        //$this->db->where('State',$state);
        $query = $this->db->get();
        foreach($query->result() as $row)
        {
            $states[]=$row;
        }
        success('All States',$states);



    }
   }


































