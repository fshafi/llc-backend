
<!DOCTYPE html>
<html>
<head>
    <title>.: Privacy Policy :.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url()?>public/css/css.css">

    <link rel="stylesheet" href="<?php echo base_url()?>public/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/bootstrap/css/b">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
    <script src="<?php echo base_url()?>public/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>public/js/bootbox.js"></script>

<style>
    h4
    {
        font-size: 17.5px;
    }
</style>




</head>
<body  >
<div class="container-fluid" id="headerbg">
    <div class="row">
        <div class="col-md-12">
            <img src="<?php echo base_url() ?>/public/images/logo-startup.png" class="img-responsive" style="padding-top:1%;padding-bottom: 1%">
        </div>
    </div>



</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br/>
            <h4>StartUp Incorporation Services respects the privacy of the users of our services and web site and we have implemented the following privacy policy</h4>
             <br/>
             <h4>Information Collected</h4>
             <p>
                 In order to complete your order through our site we are required to collect certain personal information about you including, but not limited to, name, mailing address, telephone, fax and your email address; we also may require, depending upon your order, information about the business you are incorporating through our site such as the business name, description of what the business does, information relating to directors, officer, stockholders and registered agent along with information relating to the number of shares of stock and any par value. This information is crucial to the accurate and timely processing of your order.

             </p>
            <br/>
            <h4>Confidentiality</h4>
            <p>
                All information collected is for the purposes of incorporating your new business in the appropriate state. The information you supply to us in connection with your order is confidential except we may share this information, as necessary, to execute your order or comply with state law or court order. Your credit card information will be used only to complete your order and will not be shared with any third party. We will not trade, sell, or otherwise exploit your personal information. We reserve the right to collect information with respect to the date and time of visits to our site for statistical purposes. We may also collect information, for statistical purposes and marketing and advertising strategy, about how you, the user, reached our site, e.g., from which search engine or linking service.

            </p>
            <br/>
            <h4>Emails to User</h4>
            <p>
                You, the user, will be required to provide us with an email address so that we may send you confirmation of your order and other information associated with your order. When you provide us with your email address, you will automatically be enrolled to receive any "general" updates, special offers and notices that we may issue from time to time, provided, however, that you may always, at any time, cease receiving such emails by simply clicking the "unsubscribe" link on any general email you receive from us; however, if you select our registered agent service, although you may "opt out" of receiving general emails and special offers by email, at all times, you must provide us with, and maintain, your accurate and current contact email, postal address and phone number(s) to reach your entity. We need this information, as your registered agent, to carry out your order and any and all of our obligations.

            </p>
            <br/>
            <h4>Updates to Your Information</h4>
            <p>
                You may update your information at any time by contacting us through our online form or by calling our customer service line. Again, if you have selected us to be the registered agent for your entity, then as a part of our Disclaimer (Terms of Use) you are contractually required to provide us with up-to-date, accurate contact information. You can read more about your obligation to update your information in our Disclaimer.
                StartUp Incorporation Services does not sell any personal information you provide during ordering.

            </p>
            <br/>
            <h4>Email support@startapp.biz</h4>
            <img src="<?php echo base_url() ?>/public/images/skype-logo.png"><span style="padding-left: 1%"><a href="skype:+18887723548?call "> +1 888-772-3548</a></span>
            <br>
            <br>
        </div>

    </div>
</div>
<div class="container-fluid footerprivacy">
    <div class="row">
    <h4 style="padding-left: 40%;font-size: 105%; padding-top: 1%;color:#fff;">StartUp |<span style="color: #5e5e5e"> Copyright @ LLC-Corporation</h4></span>
   </div>
</div>


</body>
</html>

