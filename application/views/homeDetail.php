<?php if(($this->session->userdata('url')==false))
{
    header("Location: " . base_url() . "loginController");
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>.: HomeDetail :.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url()?>public/css/css.css">

    <link rel="stylesheet" href="<?php echo base_url()?>public/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/bootstrap/css/b">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
    <script src="<?php echo base_url()?>public/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>public/js/bootbox.js"></script>
    <style type="text/css">

        #printable { display: none; }

        @media print
        {
            .non-printable { display: none; }
            #printable { display: block; }
        }
        table td {
            border-top: none !important;
        }

        }
        hr {
            margin-top: 13px;
            margin-bottom: 0px;
            border: 0;
            border-top: 1px solid #eee;
        }
        select:focus {
            outline-color: transparent;
        }
        .post-title
        {
            display: none !important;
        }
        @page
        {
            size: auto;   /* auto is the initial value */
            margin: 7mm;  /* this affects the margin in the printer settings */
        }
    </style>




</head>
<body class="loginBg">
<div class="container-fluid non-printable"  >
    <div class="row">
        <div class="col-md-10"style="background-color:#e45847;" >
            <div style="width: 20%;padding-top: 1%">
                <a href="<?php echo base_url();?>home"> <img src="<?php echo base_url();?>/public/images/logo-startup.png"  class="img-responsive " style="margin-left: 3%;
                             margin-top: 2%;">
                </a>
                <br/>

            </div>
        </div>
        <div class="col-md-2" style="background-color: #c94e3f;">

            <div style="padding-top: 7.5%;"  >
                <a href="<?php echo base_url();?>home/logout"><img src="<?php echo base_url();?>/public/images/logout-btn.png"  class="img-responsive " style=";margin-left: 3%;margin-top:%2"> </a>
                <br/>

            </div>
        </div><!-- logout-->
    </div>
</div>

<div class="container ">
    <div class="row">
        <div class="col-md-12 non-printable" style="background-color:  #2f3036;margin-top:3%;
                  width: 97.5%;margin-left: 1.3%; border-top-left-radius: .6em;border-top-right-radius: .6em;">

            <div class="col-md-2 non-printable" style="padding-top: 1%;">
                      <div>
                    <a href="javascript:history.back()" ><img src="<?php echo base_url(); ?>/public/images/btn-back.png"  class="img-responsive" >
                    </a>

</div>

            </div>
            <div class="col-md-7 non-printable" style="padding-top: .8%;">  <h4 style="color: #ffffff;text-align: center;padding-bottom: 1%"> State / Company</h4></div>


            <div class="col-md-3" style="padding-top: 1.5%;">
                <a href="#" onclick="window.print();return false;"> <img src="<?php echo base_url(); ?>/public/images/icon-print.png"  class="img-responsive" style="padding-left:90%;padding-top: 1%;
                    ">
                </a>
            </div>
            <br/>
            <br/>
        </div>
    </div>

    <div class="row" >
        <div class="col-md-12" >
            <div style=" background-color: #ffffff; border-bottom-left-radius: .6em;border-bottom-right-radius: .6em;" class="col-md-12">
            <div class="col-md-12" style="padding-left: 75%;padding-top: 2%;">
                <?php  if(isset($info)) { foreach($info as $m) {?>
                    <?php
                    if($m->CompanyId==$id){?>
                           <?php $a=$m->CompanyId;?>

                        <select id="mySelect" onchange="myFunction(<?php echo $a ?>)"      style="background-color:#c94e3f ;color: #f9f9f9;padding-left: 1 % ; border: 2px solid #c94e3f;border-radius: .4em" >
                            <?php if($m->StatusId==0) {?>

                                <option  value="0">Paperwork not Submitted </option>
                                <option value="1">Paperwork Submitted</option>
                                <option value="2" >Process complete</option>
                            <?php } else if($m->StatusId==1) {?>
                                <option value="1">Paperwork Submitted</option>
                                <option  value="0">Paperwork not Submitted </option>
                                <option value="2" >Process complete</option>
                            <?php } else if($m->StatusId==2) {?>
                                <option value="2" >Process complete</option>
                                <option  value="0">Paperwork not Submitted </option>
                                <option value="1">Paperwork Submitted</option>
                            <?php }?>
                        </select>
                    <?php }?>

                <?php  }}?>

            </div>
                     <div class="col-md-12">


                         <? //COMPANY NAME AD SHARE INFO ?>

                        <?php $a=1; foreach($info as $m): ?>
                            <?php
                            if($m->CompanyId==$id)
                            {

                                echo "<table  class='table  '>";



                                echo "<tr >";
                                echo "<td '><h4 style='color:#484848 '>Company Name:</h4><span style='color: #a5aaae;font-size: medium'>$m->CompanyFName</span></td>";
                                echo "<td '><h4 style='color:#484848 '>Shares:</h4><span style='color: #a5aaae;font-size: medium'>$m->CorporationShare</span></td>";

                                echo "</tr>";





                                echo "</table>";
                            //echo "<span style='color: #a5aaae;font-size: medium'>$m->CompanyFName </span>";
                            }
                            ?>
                        <?php endforeach; ?>

                     </div>






                          <? //COMPANY SUMMARY ?>
              <div class="col-md-12">
                            <h4 style="color: #484848;padding-left: .6%">CompanySummary:</h4>
                            <?php $a=1; foreach($info as $m): ?>
                                <?php
                                if($m->CompanyId==$id)
                                {
                                    echo "<table class='table  '>";
                                    echo "<tbody>";
                                    echo "<tr>";
                                    echo "<hr/>";
                                    echo "</tr>";
                                    echo "<tr>";

                                    echo "<td style='color:#a5aaae;font-size: medium;padding-right: 20%'>$m->CompanySummary</td>";
                                    echo "</tr>";


                                    echo "</tbody>";

                                    echo "</table>";
//                                    echo "<span style='color: #a5aaae;font-size: medium'>$m->CompanySummary </span>";

                                }
                                ?>
                            <?php endforeach; ?>


              </div>
                <? //COMPANY BUSINESS INFO ?>
                <div class="col-md-12">
                <h4 style="color: #484848;padding-left: .6%">BusinessInfo:</h4>
                            <?php $a=1; foreach($info as $m): ?>

                                <?php
                                if($m->CompanyId==$id)
                                {
                                    echo "<table class='table '>";
                                   echo "<tbody>";
                                    echo "<tr>";
                                    echo "<hr/>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td style='color:grey;font-size: medium'>Street</td>";
                                    echo "<td style='color:#a5aaae;font-size: medium;'>$m->Street</td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td style='color:grey;font-size: medium'>City</td>";
                                    echo "<td style='color:#a5aaae;font-size: medium;'>$m->City</td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td style='color:grey;font-size: medium'>State</td>";
                                    echo "<td style='color:#a5aaae;font-size: medium;'>$m->State</td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td style='color:grey;font-size: medium'>ZipCode</td>";
                                    echo "<td style='color:#a5aaae;font-size: medium;'>$m->ZipCode</td>";
                                    echo "</tr>";
                                    echo "<tr>";
                                    echo "<td style='color:grey;font-size: medium'>City</td>";
                                    echo "<td style='color:#a5aaae;font-size: medium;'>$m->City</td>";
                                    echo "</tr>";

                                    echo "</tbody>";

                                    echo "</table>";

                                }
                                ?>

                            <?php endforeach; ?>

                </div>



                <div class="col-md-12">





                    <h4 style="color: #484848;padding-left: .6%">MembersInfo:</h4>
                    <?php
                    $a=1; foreach($info1 as $m):{if($m->CompanyId==$id) {


                        echo "<table class='table  '>";
                        echo"<tbody>";
                        echo "<tr>";
                        echo "<hr/>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='color:grey;font-size: medium'>Member</td>";
                        echo "<td style='color:#a5aaae;font-size: medium; '>$a</td>";
                        echo "<td style='color:grey;font-size: medium'>Name</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->Name</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='color:grey;font-size: medium'>Title</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->PostName</td>";

                        echo "<td style='color:grey;font-size: medium'>City</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->City</td>";
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='color:grey;font-size: medium'>Street</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->Street</td>";
                        echo "<td style='color:grey;font-size: medium'>State</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->State</td>";
                        echo "</tr>";

                                              echo "<tr>";
                        echo "<td style='color:grey;font-size: medium'>ZipCode</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->ZipCode</td>";
                        echo "<td style='color:grey;font-size: medium'>Country</td>";
                        echo "<td style='color:#a5aaae;font-size: medium;'>$m->Country</td>";
                        echo "</tr>";

                        echo "<tr >";
                        echo "<td colspan='1' style='color:grey;font-size: medium'>Address</td>";
                        echo "<td colspan='3' style='color:#a5aaae;font-size: medium;'>$m->Address</td>";


                        echo "</tr>";
                        echo"</tbody>";
                        echo "</table>";
                        $a++;

                        ?>
                    <?php } } endforeach; ?>

                </div>
            </div>

        </div>
    </div>











<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <br/>
            <br/>
        </div>
    </div>
</div>


</body>
</html>

<script type="text/javascript">
    function myFunction(id)
    {




        var x = document.getElementById("mySelect").value;

        if(x==0)
        {
            bootbox.confirm("Do you want to change the status ?", function(result)
            {
                if(result==true)
                {
                    var url = '<?php echo site_url("home/update1/")?>' + "/" + id + "/" + x;
                    window.location.href = url;
                }
                else
                {
                    var url = '<?php echo site_url("home/detail1")?>'+ "/" + id;
                    window.location.href = url;
                    return false;
                }
            });

        }
        else if(x==1)
        {
            bootbox.confirm("Do you want to change the status ?", function(result)
            {
                if(result==true)
                {
                    var url = '<?php echo site_url("home/update1/")?>' + "/" + id + "/" + x;
                    window.location.href = url;
                }
                else
                {
                    var url = '<?php echo site_url("home/detail1")?>'+ "/" + id;
                    window.location.href = url;
                    return false;
                }
            });

        }
        else if(x==2)
        {
            bootbox.confirm("Do you want to change the status ?", function(result)
            {
                if(result==true)
                {
                    var url = '<?php echo site_url("home/update1/")?>' + "/" + id + "/" + x;
                    window.location.href = url;
                }
                else
                {
                    var url = '<?php echo site_url("home/detail1")?>'+ "/" + id;
                    window.location.href = url;
                    return false;
                }
            });
        }
        else
        {
            return false;
        }
    }
</script>