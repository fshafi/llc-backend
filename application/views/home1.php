<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <style type="text/css">

        #printable { display: none; }

        @media print
        {
            #non-printable { display: none; }
            #printable { display: block; }
        }
    </style>
</head>
<body>

<div id="non-printable">
    Your normal page contents
</div>

<div id="printable">
    Printer version
</div>


</body>
</html>
