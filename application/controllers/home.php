
<?php
/**
 * Created by PhpStorm.
 * User: PC3
 * Date: 5/7/2015
 * Time: 10:26 AM
 */
//if (!defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class Home extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->model('state_model');
        $this->output->nocache();

    }

    public function index()
    {

        $this->data();
    }


    public function data()
    {

        //$this->session->set_userdata('selected', 0);

        $config["base_url"]=base_url()."home/data";

        $config["total_rows"]=$this->state_model->c();
        $config["per_page"] =10;

        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination' style=' '>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active' '><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["info"] = $this->state_model->showdetail1($config["per_page"], $page);
          $data['selected']=0;
        $this->load->view("header");
        $this->load->view("home", $data);
        $this->load->view("footer");

        $str = 'home/data/'.$page;
        $this->session->set_userdata('url', $str);//0 == for all data

    }


    public function datanotsubmit()
    {

        //$this->session->set_userdata('selected', 1);

        $config["base_url"]=base_url()."home/datanotsubmit";

        $config["total_rows"]=$this->state_model->c1();
        $config["per_page"] =10;

        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination' style=' '>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active' '><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["info"] = $this->state_model->showdetail_notsubmit($config["per_page"], $page);
        $data['selected']=1;
        $this->load->view("header");
        $this->load->view("home", $data);
        $this->load->view("footer");

$str = 'home/datanotsubmit/'.$page;
       $this->session->set_userdata('url', $str);

    }
    public function datasubmit()
    {

        //$this->session->set_userdata('selected', 2);

        $config["base_url"]=base_url()."home/datasubmit";

        $config["total_rows"]=$this->state_model->c2();
        $config["per_page"] =10;

        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination' style=' '>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active' '><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["info"] = $this->state_model->showdetail_submit($config["per_page"], $page);

        $data['selected']=2;
        $this->load->view("header");
        $this->load->view("home", $data);
        $this->load->view("footer");


       $str = 'home/datasubmit/'.$page;
        $this->session->set_userdata('url', $str);

    }


public function process()
{

   // $this->session->set_userdata('selected', 3);

    $config["base_url"]=base_url()."home/process";

    $config["total_rows"]=$this->state_model->c3();
    $config["per_page"] =10;

    $config["uri_segment"] = 3;
    $config['full_tag_open'] = "<ul class='pagination' style=' '>";
    $config['full_tag_close'] ="</ul>";
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';
    $config['cur_tag_open'] = "<li class='disabled'><li class='active' '><a href='#'>";
    $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
    $config['next_tag_open'] = "<li>";
    $config['next_tagl_close'] = "</li>";
    $config['prev_tag_open'] = "<li>";
    $config['prev_tagl_close'] = "</li>";
    $config['first_tag_open'] = "<li>";
    $config['first_tagl_close'] = "</li>";
    $config['last_tag_open'] = "<li>";
    $config['last_tagl_close'] = "</li>";

    $this->pagination->initialize($config);
    $data["links"] = $this->pagination->create_links();
    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
    $data["info"] = $this->state_model->showdetail_process($config["per_page"], $page);
    $data['selected']=3;
    $this->load->view("header");
    $this->load->view("home", $data);
    $this->load->view("footer");


   $str = 'home/process/'.$page;
    $this->session->set_userdata('url', $str);

}



public function data1()
    {


        $this->session->set_userdata('selected', 0);

        $config["base_url"]=base_url()."home/data1";

         $config["total_rows"]=$this->state_model->count1();


        $config["per_page"] =10;

        $config["uri_segment"] = 3;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active' '><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);
        $data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["info"] = $this->state_model->showdetail2($config["per_page"], $page);

        $this->load->view("header");
        $this->load->view("home", $data);
        $this->load->view("footer");


       $str = 'home/data1/'.$page;
        $this->session->set_userdata('url', $str);

    }
    public function logOut()
    {
////        $this->session->sess_destroy();
        $this->session->unset_userdata('url');
        $this->session->sess_destroy();
        redirect(site_url('loginController'));
    }
    public function test()
    {
        $this->load->model('state_model');
        $this->state_model->test();

    }
    public function detail()
    {





            $data['id']= $this->uri->segment(3);

            $this->load->model('state_model');
            $data['info']=$this->state_model->showdetail();
            $data['info1']=$this->state_model->memberdetail();
            $this->load->view('homeDetail',$data);






        //$this->load->view('homeDetail');

    }
    public function detail1($id)
    {




        $data['id']= $id;

        $this->load->model('state_model');
        $data['info']=$this->state_model->showdetail();
        $data['info1']=$this->state_model->memberdetail();
        $this->load->view('homeDetail',$data);






        //$this->load->view('homeDetail');

    }
    public function update($C_id,$S_id)
    {

        $data = array(
            'StatusId' => $S_id,

        );

        $this->db->where('CompanyId', $C_id);
        $this->db->update('businessinfo', $data);

        $str2 = $this->session->userdata('url');
        echo $str2;
        redirect(site_url($str2));

// Produces:
// UPDATE mytable
// SET title = '{$title}', name = '{$name}', date = '{$date}'
// WHERE id = $id




    }
    public function update1($C_id,$S_id)
    {

        $data = array(
            'StatusId' => $S_id,

        );

        $this->db->where('CompanyId', $C_id);
        $this->db->update('businessinfo', $data);

       $this->detail();
// Produces:
// UPDATE mytable
// SET title = '{$title}', name = '{$name}', date = '{$date}'
// WHERE id = $id




    }
    public function getAllStates()
    {
        $query=$this->db->get('States');
        foreach($query->result() as $row)
        {
            $states=$row;
        }
        success('All States',$states);



    }



} 