<?php
/**
 * Created by PhpStorm.
 * User: PC3
 * Date: 5/7/2015
 * Time: 10:28 AM
 */
ob_start();
class LoginController extends CI_Controller
{
    public function index()
    {


          $this->load->model('state_model');
        if ($this->session->userdata('UserName') == true) {
            redirect(site_url('home'));

        } else {

            $this->load->view('loginPage');
        }


    }


    public function adminLogin()
    {

        $this->load->model('state_model');
        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $this->form_validation->set_rules('username', 'username', 'trim|xss_clean|required');
        $this->form_validation->set_rules('password', 'password', 'trim|xss_clean|required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == true)
        {
            if($this->state_model->attemptLogin($username,$password))
            {

                $this->session->set_userdata('UserName', $username);

                redirect(site_url('home'));
            }
            else
            {
                $data['error']='Invalid Login';
                $this->load->view('loginPage',$data);
            }



        }
        else
        {
            $this->load->view('loginPage');
        }


    }

} 