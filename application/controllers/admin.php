<?php

class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $this->load->view('landingPage');
    }
    public function logOut()
    {
        $this->session->sess_destroy();
        redirect(site_url('login'));
    }

}
