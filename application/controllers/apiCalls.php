<?php
/**
 * Created by PhpStorm.
 * User: DELL OPTIPLEX 380
 * Date: 4/16/2015
 * Time: 10:53 AM
 */


class ApiCalls extends CI_Controller
{
######################################################################################################################
                                     //   GET ALL DETAIL (1)
######################################################################################################################

    public function getDetail()
    {
        $this->load->model('state_model');
        $this->state_model->getDetail();


    }

######################################################################################################################
                            //   ADD COMPANY BUSINESS INFORMATION (2)
######################################################################################################################

    public  function addInfo()

    {
        if ($this->form_validation->run() == FALSE) {
           error("error");
           // echo "error";
        } else {
            $this->load->model('state_model');
            $this->state_model->addAllInfo();

        }

    }



######################################################################################################################
                            // GET COMPANY BUSINESS INFORMATION (3)
######################################################################################################################

        public function getBusinessInfo()
        {
            $this->load->model('state_model');
            $this->state_model->getBusinessInfo();
        }

######################################################################################################################
                               // ADD COMPANY MEMBERS INFORMATION (4)
######################################################################################################################


######################################################################################################################
                      // GET COMPANY MEMBERS INFORMATION (5)
######################################################################################################################

    public function getCompanyMember()
    {
        $this->load->model('state_model');
        $this->state_model->getCompanyMember();

    }


######################################################################################################################
                // GET COMPANY MEMBERS INFORMATION (7)
######################################################################################################################

    public function getBusinessCategory()
    {
        $this->load->model('state_model');
        $this->state_model->getBusinessCategory();
    }

    public function getAllDetail()
    {

        $this->load->model('state_model');
        $this->state_model->getAllDetail();


}
//    public function test()
//    {
//
//        $this->load->model('state_model');
//        $this->state_model->test();
//
//
//    }
    public function getExpediteInfo()
    {
        $this->load->model('state_model');
        $this->state_model->getExpedite();

    }
    public function forgetPassword()
    {
        $this->load->model('state_model');
        $this->state_model->forgetPassword();

    }
    public function ExpediteInfo()
    {
        $this->load->model('state_model');
        $this->state_model->Expedite();

    }
    public function createBusinessAccount()
    {


            $this->load->model('state_model');
            $this->state_model->createBusinessAccount();


    }
    public function addCompanyMembers()
    {



            $this->load->model('state_model');
            $this->state_model->addCompanyMembers();


    }
    public function addBusinessCategory()
    {

            $this->load->model('state_model');
            $this->state_model->addBusinessCategory();



    }
    public function addAllInfo()
    {


//        if ($this->form_validation->run() == FALSE) {
//            error("validation Error");
//            // echo "error";
//        } else {
            $this->load->model('state_model');
            $this->state_model->addAllInfo();

    //    }


    }
    public function register()
    {
        $this->load->model('state_model');
        $this->state_model->register();

     }
    public function applicationInfo()
{
    $this->load->model('state_model');
    $this->state_model->applicationInfo();

}
    public function stripePayment()
    {
        $this->load->model('state_model');
        $this->state_model->stripePayment();
    }
    public function payment()
    {
        $this->load->model('state_model');
        $this->state_model->payment();
    }
    public function chAddress()
    {
        $this->load->model('state_model');
        $this->state_model->chAddress();

    }
    public function getId()
    {
        $this->load->model('state_model');
      echo $this->state_model->getId();


    }
    public function login()
    {
        $this->load->model('state_model');
       $this->state_model->login();


    }
    public function companySA()
    {

        $this->load->model('state_model');
        $this->state_model->companySA();
    }

    public function stripePaymentPage()
    {
        $this->load->model('state_model');
        $this->state_model->stripePaymentPage();
    }
    public function stripePaymentOperation()
    {
        $this->load->model('state_model');
        $this->state_model->stripePaymentOperation();

    }

    public function updateToken()
    {
        $this->load->model('state_model');
        $this->state_model->updateToken();
    }
    public function getAllStates()

    {
        $this->load->model('state_model');
        $this->state_model->getAllStates();


}
    public function stateFeesInfo()

    {
        $this->load->model('state_model');
        $this->state_model->stateFeesInfo();


    }
}